# Dashboard Devtools
This tool is for inserting test-data into the database that fits the TPC-H benchmark specifications.

## Usage
```
npx ts-node src/index.ts --table=<name> --sf=<number>
```
where table can be: `customer`, `orders`, `supplier`, `part`, `partsupp`, and `lineitem` and sf is default `1`.
