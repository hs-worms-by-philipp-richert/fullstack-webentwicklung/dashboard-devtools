// SCRIPT FOR INSERTING TEST DATA
// USING TPC-H SPECIFICATION
// USAGE: npx ts-node src/index.ts --table=<name> --sf=<number>

import { AppDataSource } from './data-source';
import { BaseDataService } from './interfaces/baseDataService.interface';
import {
  CustomerDataService,
  LineItemDataService,
  OrdersDataService,
  PartDataService,
  PartSuppDataService,
  SupplierDataService
} from './services';

// Establish database connection
AppDataSource.initialize()
  .then(async () => {
    console.log('Data Source has been initialized!');

    try {
      await initData();
    } catch (error) {
      console.error((error as Error).message);
      process.exit(1);
    }
  })
  .catch((error) => console.error('Error during Data Source initialization:', error));

async function initData() {
  console.log('\nTest data has been sourced from official TPC sources\n');
  
  const tableParameter: string[] | undefined = process.argv.slice(2).find(r => r.startsWith('--table='))?.split('=');
  let sfParameter: string[] | undefined = process.argv.slice(2).find(r => r.startsWith('--sf='))?.split('=');

  if (tableParameter === undefined) {
    throw new Error('Wrong command usage. Please call command with --table=<value> parameter.');
  }

  if (sfParameter === undefined) {
    sfParameter = ['sf', '1'];
  }

  let dataService: BaseDataService | null;

  switch (tableParameter[1]) {
    case 'customer':
      dataService = new CustomerDataService(tableParameter[1], sfParameter[1]);
      break;
    case 'orders':
      dataService = new OrdersDataService(tableParameter[1], sfParameter[1]);
      break;
    case 'supplier':
      dataService = new SupplierDataService(tableParameter[1], sfParameter[1]);
      break;
    case 'part':
      dataService = new PartDataService(tableParameter[1], sfParameter[1]);
      break;
    case 'partsupp':
      dataService = new PartSuppDataService(tableParameter[1], sfParameter[1]);
      break;
    case 'lineitem':
      dataService = new LineItemDataService(tableParameter[1], sfParameter[1]);
      break;
    default:
      throw new Error('Incorrect table name. Tables are named after TPC-H specification.');
  }

  const data: object[] | undefined = await dataService?.generateData();

  if (data === undefined) {
    throw new Error('An error occurred when attempting to create the dataset.');
  }

  const res: boolean = await dataService?.insertData(data) ?? false;

  if (res) {
    console.log('Success! Data has been saved to the database.');
    process.exit();
  } else {
    console.log('Unable to save data. Check your database.');
    process.exit(1);
  }
}
