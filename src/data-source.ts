import 'reflect-metadata';
import { DataSource } from 'typeorm';
import { Customer } from './entity/Customer';
import { LineItem } from './entity/LineItem';
import { Nation } from './entity/Nation';
import { Order } from './entity/Order';
import { Part } from './entity/Part';
import { PartSupp } from './entity/PartSupp';
import { Region } from './entity/Region';
import { Supplier } from './entity/Supplier';

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'fullstack',
  password: 'verySecurePassword',
  database: 'fullstack',
  synchronize: true,
  logging: true,
  entities: [
    Customer,
    LineItem,
    Nation,
    Order,
    Part,
    PartSupp,
    Region,
    Supplier
  ],
  subscribers: [],
  migrations: []
});
