import { AppDataSource } from "../data-source";
import { Customer } from "../entity/Customer";
import { Order } from "../entity/Order";
import { Part } from "../entity/Part";
import { PartSupp } from "../entity/PartSupp";
import { Supplier } from "../entity/Supplier";

export type PartSuppObj = {
  partkey: bigint,
  suppkey: bigint
};

export const getUUIDs = async (): Promise<string[]> => {
  const uuidList: Customer[] = await AppDataSource.getRepository(Customer)
    .createQueryBuilder('customer')
    .select('custkey')
    .getRawMany();

  return uuidList.map(m => m.custkey);
}

export const getParts = async (): Promise<bigint[]> => {
  const partkeyList: Part[] = await AppDataSource.getRepository(Part)
    .createQueryBuilder('part')
    .select('partkey')
    .getRawMany();

  return partkeyList.map(m => m.partkey);
}

export const getSupps = async (): Promise<bigint[]> => {
  const partkeyList: Supplier[] = await AppDataSource.getRepository(Supplier)
    .createQueryBuilder('supplier')
    .select('suppkey')
    .getRawMany();

  return partkeyList.map(m => m.suppkey);
}

export const getPartSupps = async (): Promise<PartSuppObj[]> => {
  const list: PartSuppObj[] = await AppDataSource.getRepository(PartSupp)
    .createQueryBuilder('part_supp')
    .select('partkey')
    .addSelect('suppkey')
    .getRawMany();

  return list;
}

export const getOrders = async (): Promise<bigint[]> => {
  const orderList: Order[] = await AppDataSource.getRepository(Order)
    .createQueryBuilder('order')
    .select('orderkey')
    .getRawMany();

  return orderList.map(m => m.orderkey);
}

export const getRandomIndex = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const renewOldDates = (dataset: string[], index: number): string[] => {
  let years: number[] = [];
  let dates: string[][] = [];
  let dateRegEx: RegExp = /\|([0-9]{4})-(1[0-2]|0[1-9])-(0[1-9]|[1-2][0-9]|3[0-1])/g;

  for (let i = 0; i < dataset.length; i++) {
    const row: string = dataset[i];
    const match: RegExpMatchArray[] = [...row.matchAll(dateRegEx)];

    const year: string = match[index][1];
    const date: string[] = [match[index][1], match[index][2], match[index][3]];
    
    if (year !== undefined && !Number.isNaN(year)) {
      years.push(Number(year));
      dates.push(date);
    }
  }

  let highestYear: number = Math.max.apply(null, years);
  let yearDiff = (new Date).getFullYear() - (highestYear);

  return dates.map(d => `${Number(d[0]) + yearDiff}-${d[1]}-${d[2]}`);
}
