import fs from 'fs';

export const readFileAsArray: Function = (path: string): string[] => {
  let array: string[] = fs.readFileSync(path, 'utf-8').toString().split('\n');

  return array.filter(str => str.length);
}
