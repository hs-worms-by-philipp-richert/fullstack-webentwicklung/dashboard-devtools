import { AppDataSource } from "../data-source";
import { PartSupp } from "../entity/PartSupp";
import { BaseDataService } from "../interfaces/baseDataService.interface";
import { getParts, getRandomIndex, getSupps } from "../utility/dataset.helper";
import { readFileAsArray } from "../utility/file.helper";

export class PartSuppDataService implements BaseDataService {
  private partkeys: bigint[] = [];
  private suppkeys: bigint[] = [];

  constructor(private tableName: string, private sf: string) {
  }

  async generateData(): Promise<object[]> {
    let rawData: string[] = readFileAsArray(`./data/${this.tableName}.tbl.${this.sf}`);
    let data: object[] = [];

    this.partkeys = await getParts();
    this.suppkeys = await getSupps();

    let partsIterator: number = 0;

    for (let i = 0; i < rawData.length; i++) {
      let row: string[] = rawData[i].split('|');

      data.push({
        partkey: this.partkeys[partsIterator],
        suppkey: this.suppkeys[getRandomIndex(0, this.suppkeys.length-1)],
        availqty: Number(row[2]),
        supplycost: Number(row[3])
      });

      partsIterator++;

      if (partsIterator >= this.partkeys.length) {
        partsIterator = 0;
      }
    }

    return data;
  }

  async insertData(data: object[]): Promise<boolean> {
    const res: object[] = await AppDataSource.getRepository(PartSupp).save(data, { chunk: data.length / 10 });

    return res.length > 0;
  }
}
