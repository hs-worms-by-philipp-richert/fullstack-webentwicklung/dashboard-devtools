import { AppDataSource } from "../data-source";
import { Customer } from "../entity/Customer";
import { BaseDataService } from "../interfaces/baseDataService.interface";
import { readFileAsArray } from "../utility/file.helper";

export class CustomerDataService implements BaseDataService {
  constructor(private tableName: string, private sf: string) {
  }

  async generateData(): Promise<object[]> {
    let rawData: string[] = readFileAsArray(`./data/${this.tableName}.tbl.${this.sf}`);
    let data: object[] = [];

    for (let i = 0; i < rawData.length; i++) {
      let row: string[] = rawData[i].split('|');

      data.push({
        name: row[1],
        address: row[2],
        nationkey: Number(row[3])+1,
        phone: row[4],
        acctbal: Number(row[5]),
        mktsegment: row[6]
      });
    }

    return data;
  }

  async insertData(data: object[]): Promise<boolean> {
    const res: object[] = await AppDataSource.getRepository(Customer).save(data, { chunk: data.length / 10 });

    return res.length > 0;
  }
}
