import { AppDataSource } from "../data-source";
import { Part } from "../entity/Part";
import { BaseDataService } from "../interfaces/baseDataService.interface";
import { readFileAsArray } from "../utility/file.helper";

export class PartDataService implements BaseDataService {
  constructor(private tableName: string, private sf: string) {
  }

  async generateData(): Promise<object[]> {
    let rawData: string[] = readFileAsArray(`./data/${this.tableName}.tbl.${this.sf}`);
    let data: object[] = [];

    for (let i = 0; i < rawData.length; i++) {
      let row: string[] = rawData[i].split('|');

      data.push({
        name: row[1],
        mfgr: row[2],
        brand: row[3],
        type: row[4],
        size: Number(row[5]),
        container: row[6],
        retailprice: Number(row[7])
      });
    }

    return data;
  }

  async insertData(data: object[]): Promise<boolean> {
    const res: object[] = await AppDataSource.getRepository(Part).save(data, { chunk: data.length / 10 });

    return res.length > 0;
  }
}
