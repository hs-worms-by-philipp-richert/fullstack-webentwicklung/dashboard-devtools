import { AppDataSource } from "../data-source";
import { LineItem } from "../entity/LineItem";
import { BaseDataService } from "../interfaces/baseDataService.interface";
import { getOrders, getPartSupps, PartSuppObj, renewOldDates } from "../utility/dataset.helper";
import { readFileAsArray } from "../utility/file.helper";

export class LineItemDataService implements BaseDataService {
  private partsupps: PartSuppObj[] = [];
  private orderkeys: bigint[] = [];

  constructor(private tableName: string, private sf: string) {
  }

  async generateData(): Promise<object[]> {
    let rawData: string[] = readFileAsArray(`./data/${this.tableName}.tbl.${this.sf}`);
    let data: object[] = [];

    this.partsupps = await getPartSupps();
    this.orderkeys = await getOrders();

    let newShipDates: string[] = renewOldDates(rawData, 0);
    let newCommitDates: string[] = renewOldDates(rawData, 1);
    let newReceiptDates: string[] = renewOldDates(rawData, 2);

    let partsuppsIterator: number = 0;
    let orderIterator: number = this.orderkeys.length-1;

    for (let i = 0; i < rawData.length; i++) {
      let row: string[] = rawData[i].split('|');

      data.push({
        orderkey: this.orderkeys[orderIterator],
        partkey: this.partsupps[partsuppsIterator].partkey,
        suppkey: this.partsupps[partsuppsIterator].suppkey,
        quantity: Number(row[4]),
        extendedprice: Number(row[5]),
        discount: Number(row[6]),
        tax: Number(row[7]),
        returnflag: row[8],
        linestatus: row[9],
        shipdate: newShipDates[i],
        commitdate: newCommitDates[i],
        receiptdate: newReceiptDates[i],
        shipinstruct: row[13],
        shipmode: row[14]
      });

      partsuppsIterator++;
      orderIterator--;

      if (partsuppsIterator >= this.partsupps.length) {
        partsuppsIterator = 0;
      }

      if (orderIterator < 0) {
        orderIterator = this.orderkeys.length-1;
      }
    }

    return data;
  }

  async insertData(data: object[]): Promise<boolean> {
    const res: object[] = await AppDataSource.getRepository(LineItem).save(data, { chunk: data.length / 10 });

    return res.length > 0;
  }
}
