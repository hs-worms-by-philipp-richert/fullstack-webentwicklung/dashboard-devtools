import { AppDataSource } from "../data-source";
import { Customer } from "../entity/Customer";
import { Order } from "../entity/Order";
import { BaseDataService } from "../interfaces/baseDataService.interface";
import { getUUIDs, getRandomIndex, renewOldDates } from "../utility/dataset.helper";
import { readFileAsArray } from "../utility/file.helper";
import { formatDate } from "../utility/format.helper";

type OrdersObj = {
  custkey: string,
  orderstatus: string,
  totalprice: number,
  orderdate: string,
  orderpriority: string,
  clerk: string,
  shippriority: number
};

export class OrdersDataService implements BaseDataService {
  private uuids: string[] = [];

  constructor(private tableName: string, private sf: string) {
  }

  async generateData(): Promise<object[]> {
    let rawData: string[] = readFileAsArray(`./data/${this.tableName}.tbl.${this.sf}`);
    let data: OrdersObj[] = [];
    
    let newDates: string[] = renewOldDates(rawData, 0);

    this.uuids = await getUUIDs();

    for (let i = 0; i < rawData.length; i++) {
      let row: string[] = rawData[i].split('|');

      data.push({
        custkey: this.uuids[getRandomIndex(0, this.uuids.length-1)],
        orderstatus: row[2],
        totalprice: Number(row[3]),
        orderdate: newDates[i],
        orderpriority: row[5],
        clerk: row[6],
        shippriority: Number(row[7])
      });
    }

    // edit data for dev reasons
    let newDate1: Date = new Date();
    let newDate2: Date = new Date();
    let newDate3: Date = new Date();
    let newDate4: Date = new Date();
    let newDate5: Date = new Date();
    let newDate6: Date = new Date();
    let newDate7: Date = new Date();

    newDate2.setDate(newDate2.getDate() - 5);
    newDate3.setDate(newDate3.getDate() - 10);
    newDate4.setMonth(newDate1.getMonth() - 1);
    newDate5.setMonth(newDate2.getMonth() - 1);
    newDate6.setMonth(newDate1.getMonth() - 2);
    newDate7.setMonth(newDate2.getMonth() - 2);
    
    data[data.length-1].orderdate = formatDate(newDate1);
    data[data.length-2].orderdate = formatDate(newDate2);
    data[data.length-3].orderdate = formatDate(newDate3);
    data[data.length-4].orderdate = formatDate(newDate4);
    data[data.length-5].orderdate = formatDate(newDate5);
    data[data.length-6].orderdate = formatDate(newDate6);
    data[data.length-7].orderdate = formatDate(newDate7);

    return data;
  }

  async insertData(data: object[]): Promise<boolean> {
    const res: object[] = await AppDataSource.getRepository(Order).save(data, { chunk: data.length / 10 });

    return res.length > 0;
  }
}
