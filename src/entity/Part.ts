import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Part {
  @PrimaryGeneratedColumn({ type: "bigint" })
  partkey: bigint

  @Column("varchar", { length: 55 })
  name: string

  @Column("char", { length: 25 })
  mfgr: string

  @Column("char", { length: 10 })
  brand: string

  @Column("varchar", { length: 25 })
  type: string

  @Column("int")
  size: number

  @Column("char", { length: 10 })
  container: string

  @Column("numeric")
  retailprice: number

  @Column("varchar", { length: 23, nullable: true })
  comment: string
}
