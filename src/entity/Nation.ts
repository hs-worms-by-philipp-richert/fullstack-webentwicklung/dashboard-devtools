import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Region } from './Region';

@Entity()
export class Nation {
  @PrimaryGeneratedColumn({ type: "int" })
  nationkey: number

  @Column("char", { length: 25 })
  name: string

  @ManyToOne(() => Region, region => region.regionkey)
  @JoinColumn({ name: "regionkey" })
  @Column("int")
  regionkey: number

  @Column("varchar", { length: 152, nullable: true })
  comment: string
}
