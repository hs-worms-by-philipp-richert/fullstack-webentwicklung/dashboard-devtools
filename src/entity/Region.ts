import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Region {
  @PrimaryGeneratedColumn({ type: "int" })
  regionkey: number

  @Column("char", { length: 25 })
  name: string

  @Column("varchar", { length: 152, nullable: true })
  comment: string
}
