import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Part } from './Part';
import { Supplier } from './Supplier';

@Entity()
export class PartSupp {
  @ManyToOne(() => Part, part => part.partkey)
  @JoinColumn({ name: "partkey" })
  @PrimaryColumn({ type: "bigint" })
  partkey: bigint

  @ManyToOne(() => Supplier, supplier => supplier.suppkey)
  @JoinColumn({ name: "suppkey" })
  @PrimaryColumn({ type: "bigint" })
  suppkey: bigint

  @Column("int")
  availqty: number

  @Column("numeric")
  supplycost: number

  @Column("varchar", { length: 199, nullable: true })
  comment: string
}
