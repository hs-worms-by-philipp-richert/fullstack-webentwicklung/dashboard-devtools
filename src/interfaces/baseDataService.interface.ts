export interface BaseDataService {
  generateData(): Promise<object[]>;

  insertData(data: object): Promise<boolean>;
}
